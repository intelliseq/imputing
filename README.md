# imputing

#### How to build the containers:
First run the command in this directory to get the necessary files (reference files, binaries etc.)
```bash
docker run -v $(pwd):/app_data wgalan/imputing_get_necessary_files:1
```
Then run these commands in this directory to build containers:
```bash
docker build --no-cache -t wgalan/imputing_vcf_input:1 . -f docker/intelliseqarray/imputing_vcf_input/1.0/Dockerfile && \
docker build --no-cache -t wgalan/imputing_eagle:1 . -f docker/intelliseqarray/imputing_eagle/1.0/Dockerfile && \
docker build --no-cache -t wgalan/imputing_beagle:1 . -f docker/intelliseqarray/imputing_beagle/1.0/Dockerfile
```

#### how to run the containers:
```bash
docker run -v path/to/the/data/directory:/app_data wgalan/imputing_vcf_input:1 vcf_input_file genotype_dir --log_dir log_dir --hg19 &&
docker run -v path/to/the/data/directory:/app/data wgalan/imputing_eagle:1 genotype_dir phased_genotype_dir log_dir --eagle_threads num && \
docker run -v path/to/the/data/directory:/app/data wgalan/imputing_beagle:1 phased_genotype_dir imputed_genotype_dir log_dir --beagle_threads num --hg19 #TODO sample info
```
for example:
```bash
docker run -v /home/wojtek/PycharmProjects/imputing/example_data:/app_data wgalan/imputing_vcf_input:1 /app_data/genotyping_workflow_vcf.vcf /app_data/initial_genotype --log_dir /app_data/logs --hg19 && \
docker run -v /home/wojtek/PycharmProjects/imputing/example_data:/app_data wgalan/imputing_eagle:1 /app_data/initial_genotype /app_data/phased_genotype /app_data/logs --eagle_threads 4 && \
docker run -v /home/wojtek/PycharmProjects/imputing/example_data:/app_data wgalan/imputing_beagle:1 /app_data/phased_genotype /app_data/imputed_genotype /app_data/logs --beagle_threads 4 --hg19 #TODO sample info
```
