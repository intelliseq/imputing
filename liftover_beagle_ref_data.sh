#!/usr/bin/env bash

DATA_DIRECTORY="/home/wojtek/beagle_ref_data"
VENV_DIRECTORY="/tmp/crossmap_venv"
concatenated="concatenated.vcf.gz"
liftovered="liftovered.vcf"
header="header"
bref3_path="/home/wojtek/.local/bin/bref3.16May19.351.jar"
cd $DATA_DIRECTORY
if ! [[ -f ${bref3_path} ]]; then
    wget https://faculty.washington.edu/browning/beagle/bref3.16May19.351.jar
    mv bref3.16May19.351.jar $bref3_path
fi
if ! [[ -f ${header} ]]; then
    zgrep '^#' chr22.1kg.phase3.v5a.vcf.gz > $header
fi
if ! [[ -f ${concatenated} ]]; then
    zcat $(ls -d chr*.1kg.phase3.v5a.vcf.gz) | grep -v "#" | gzip > /tmp/$concatenated
    cat <(gzip $header) /tmp/$concatenated > $concatenated
    rm /tmp/$concatenated
fi
#exit 0
if ! [[ -f ${liftovered} ]]; then
    virtualenv $VENV_DIRECTORY -p /usr/bin/python3.6
    source $VENV_DIRECTORY/bin/activate
    pip3 install --upgrade git+https://github.com/wojciech-galan/CrossMap biopython
    CrossMap.py vcf ~/crossmap_files/hg19ToHg38.over.chain.gz $concatenated /data/repositories/workflows/src/main/resources/reference-genomes/broad-institute-hg38/Homo_sapiens_assembly38.fa $liftovered --discard_unmapped
    rm ${liftovered}.unmap
fi

grep -v "#" ${liftovered} | awk '{print >$1".38.vcf"}'

# sort vcfs, gzip'em all, find duplicated
for chr in {1..22} X; do
  echo "Processing chrom $chr"
  fname=${chr}.38.vcf
  fname_sorted=${fname}.sorted
  fname_duplicated=${fname}.duplicated
  fname_final=${fname}.final
  fname_final_sorted=${fname}.final.sorted
  fname_invalid=${fname}.invalid
  fname_bref=${chr}.38.bref3
  if ! [[ -f ${fname_sorted} ]]; then
    sort -k2,2 -n $fname > ${fname_sorted}
  fi
  if ! [[ -f ${fname_duplicated} ]]; then
    cut -f2 ${fname_sorted} |uniq -d > ${fname_duplicated}
  fi
  if ! [[ -f ${fname_final} ]]; then
    python3 /home/wojtek/remove_duplicated.py ${fname_sorted} ${fname_final} ${fname_duplicated} --invalid_out $fname_invalid --python_path $VENV_DIRECTORY/bin/python3
  fi
  if ! [[ -f ${fname_final_sorted} ]]; then
    sort -k2,2 -n $fname_final > ${fname_final_sorted}
  fi
done
for chr in {1..22}; do
    cat $header $fname_final_sorted | java -jar $bref3_path > $fname_bref
done
cat $header X.38.vcf.final.sorted > X.38.vcf

# rm files
rm {1..9}*.vcf
rm {Y,U}*.vcf # do not remove X.38.vcf!
rm {1..9,X}*.vcf.duplicated
rm {1..9,X,Y,U}*.vcf.final*
rm $concatenated
rm $liftovered
rm $header
deactivate
rm -rf $VENV_DIRECTORY
cd ..
