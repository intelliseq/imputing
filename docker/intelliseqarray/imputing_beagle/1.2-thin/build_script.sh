#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -rf $tempdir/*
else
    mkdir $tempdir
fi

mkdir $tempdir/bin
ln bin/beagle.16May19.351.jar $tempdir/bin
ln requirements.txt $tempdir/
cp -r imputing $tempdir/
docker build --no-cache -t intelliseqarray/imputing_beagle:1.2-thin $tempdir -f docker/intelliseqarray/imputing_beagle/1.2-thin/Dockerfile

rm -rf $tempdir
