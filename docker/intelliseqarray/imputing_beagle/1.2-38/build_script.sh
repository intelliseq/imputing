#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -rf $tempdir/*
else
    mkdir $tempdir
fi

mkdir $tempdir/bin
mkdir $tempdir/data
mkdir $tempdir/data/beagle_genetic_maps
mkdir $tempdir/data/beagle_ref_data
ln bin/beagle.16May19.351.jar $tempdir/bin
ln requirements.txt $tempdir/
cp -r imputing $tempdir/
ln data/beagle_genetic_maps_38/* $tempdir/data/beagle_genetic_maps/
ln data/beagle_ref_38/* $tempdir/data/beagle_ref_data/
docker build --no-cache -t intelliseqarray/imputing_beagle:1.2-38 $tempdir -f docker/intelliseqarray/imputing_beagle/1.2-38/Dockerfile

rm -rf $tempdir
