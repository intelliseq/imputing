#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -rf $tempdir/*
else
    mkdir $tempdir
fi

mkdir $tempdir/data
mkdir $tempdir/data/beagle_genetic_maps
mkdir $tempdir/data/beagle_ref_data
mkdir $tempdir/data/eagle_ref_data
ln data/Axiom_PMDA.na36.r6.a8.annot.csv $tempdir/data
ln ../mobigen/src/main/resources/databases/allele_frequency_db.sqlite $tempdir/data
ln data/beagle_genetic_maps/* $tempdir/data/beagle_genetic_maps/
ln data/beagle_ref_data/* $tempdir/data/beagle_ref_data/
ln data/genetic_map_hg19_withX.txt.gz $tempdir/data
ln data/eagle_ref_data/* $tempdir/data/eagle_ref_data/

docker build --no-cache -t intelliseqarray/imputing_ref_data:1.1 $tempdir -f docker/intelliseqarray/imputing_ref_data/1.1/Dockerfile

rm -rf $tempdir