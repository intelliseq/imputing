#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -r $tempdir/*
else
    mkdir $tempdir
fi

mkdir $tempdir/data
ln data/Axiom_PMDA.na36.r6.a8.annot.csv $tempdir/data
cp -r imputing $tempdir/
ln requirements.txt $tempdir/
docker build --no-cache -t intelliseqarray/imputing_vcf_input:1.2 $tempdir -f docker/intelliseqarray/imputing_vcf_input/1.2/Dockerfile

rm -r $tempdir