#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -r $tempdir/*
else
    mkdir $tempdir
fi

cp -r imputing $tempdir/
ln requirements.txt $tempdir/
docker build --no-cache -t intelliseqarray/imputing_vcf_input:1.3.1-thin $tempdir -f docker/intelliseqarray/imputing_vcf_input/1.3.1-thin/Dockerfile

rm -r $tempdir