#!/usr/bin/env bash

# should be executed from project-level directory
tempdir="/tmp/docker"
if [[ -d ${tempdir} ]]; then
    rm -r $tempdir/*
else
    mkdir $tempdir
fi

mkdir $tempdir/bin
mkdir $tempdir/data
mkdir $tempdir/data/eagle_ref_data
ln bin/eagle $tempdir/bin
cp -r imputing $tempdir/
ln data/genetic_map_hg38_withX.txt.gz $tempdir/data
ln data/eagle_ref_38/* $tempdir/data/eagle_ref_data/
docker build --no-cache -t intelliseqarray/imputing_eagle:1.0-38 $tempdir -f docker/intelliseqarray/imputing_eagle/1.0-38/Dockerfile

rm -r $tempdir