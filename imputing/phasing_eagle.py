import logging
import argparse
import os
from queue import Queue
from libs.phasing import compress_vcf
from libs.phasing import make_index_for_compressed_vcf
from libs.phasing import run_eagle

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Performs phasing with Eagle_v2.4.1')
    parser.add_argument('in_dir', type=str, help='directory containing input files (chrom.vcf)')
    parser.add_argument('out_dir', type=str, help='directory for output (phased) files')
    parser.add_argument('log_dir', type=str, help='directory for logs')
    parser.add_argument('--eagle_threads', type=int, help='Number of threads to be used by eagle', default=1)
    parser.add_argument('--eagle_ref_data_dir', type=str, default='/app_data/eagle_ref_data',
                        help='Directory containing eagle reference data')
    parser.add_argument('--eagle_genetic_map_path', type=str, default='/app_data/genetic_map_hg19_withX.txt.gz',
                        help='Path to eagle genetic map')
    parser.add_argument('--eagle_path', type=str, default='/app/bin/eagle', help='Path to eagle binary')
    parsed_args = parser.parse_args()
    out_dir = os.path.abspath(os.path.expanduser(parsed_args.out_dir))
    in_dir = os.path.abspath(os.path.expanduser(parsed_args.in_dir))
    log_dir = os.path.abspath(os.path.expanduser(parsed_args.log_dir))
    eagle_log_directory = os.path.join(log_dir, 'eagle')
    bgzip_log_directory = os.path.join(log_dir, 'bgzip')
    tabix_log_directory = os.path.join(log_dir, 'tabix')

    info_queue = Queue()
    for directory in [log_dir, out_dir, eagle_log_directory, bgzip_log_directory, tabix_log_directory]:
        try:
            os.makedirs(directory)
        except FileExistsError:
            info_queue.put('{} already exists'.format(directory))

    log_file = os.path.join(log_dir, '{}.log'.format(os.path.basename(__file__).split('.')[0]))
    # setting up logger
    logger = logging.getLogger('imputing')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    while not info_queue.empty():
        logger.debug(info_queue.get())

    logger.info('phasing')
    for chrom in list(range(1, 23)) + ['X']:
        uncompressed_path = os.path.join(in_dir, '{}.vcf'.format(chrom))
        compressed_path = uncompressed_path + '.gz'
        compress_vcf(uncompressed_path, compressed_path,
                     err_log_path=os.path.join(bgzip_log_directory, '{}_err'.format(chrom)))
        make_index_for_compressed_vcf(compressed_path,
                                      out_log_path=os.path.join(tabix_log_directory, '{}_out'.format(chrom)),
                                      err_log_path=os.path.join(tabix_log_directory, '{}_err'.format(chrom)))
        run_eagle(
            target_path=compressed_path,
            ref_path=os.path.join(parsed_args.eagle_ref_data_dir, '{}.bcf'.format(chrom)),
            out_path=os.path.join(out_dir, '{}_phased'.format(chrom)),
            eagle_path=parsed_args.eagle_path,
            genetic_map_path=parsed_args.eagle_genetic_map_path,
            num_threads=parsed_args.eagle_threads,
            out_log_path=os.path.join(eagle_log_directory, '{}_out'.format(chrom)),
            err_log_path=os.path.join(eagle_log_directory, '{}_err'.format(chrom))
        )
