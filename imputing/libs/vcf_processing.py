import os
import subprocess
from collections import namedtuple
from typing import Dict
from typing import List

_metadata_template = """##fileformat=VCFv4.1
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t{}\n"""

RefDataRec = namedtuple('RefDataRec',
                        'probe_set_id rsid chr pos strand allele_a allele_b ref alt hg19_chr hg19_pos hg19_strand')  # TODo does not fit to vcf_processing. Move somewhere else?

grep_lines_command = """grep -v '^["#]' {} | awk '$7 == "PASS" {{print}}' """


class VcfWriter(object):

    def __init__(self, path: str, sample_names: List[str], mode: str = 'w',
                 metadata_template: str = _metadata_template):
        super().__init__()
        self.handle = open(path, mode)
        self.handle.write(metadata_template.format('\t'.join(sample_names)))

    def write(self, splitted_record_line: List[str]):
        self.handle.write('\t'.join(splitted_record_line))
        self.handle.flush()

    def writen(self, splitted_record_line: List[str]):
        self.write(splitted_record_line)
        self.handle.write('\n')

    def __del__(self):
        self.handle.close()

    def close(self):
        del self


def curate_vcf(in_path: str, curated_file_path: str, sample_names: List[str],
               metadata_template: str = _metadata_template, cmd=grep_lines_command):
    metadata = metadata_template.format(('\t'.join(sample_names)))
    output = subprocess.check_output(cmd.format(in_path), shell='/bin/bash').decode('utf-8')
    with open(curated_file_path, 'w') as fout:
        fout.write(metadata)
        fout.write(output)


def split_vcf_by_chromosomes(vcf_file_path: str, writers: Dict[str, VcfWriter], lift_over_to_hg19: bool,
                             ref_data: Dict[str, RefDataRec], logger, compressed=False):
    """
    Splits vcf file by chromosome and performs liftover to hg19 if needed
    :param vcf_file_path: vcf file to be processed
    :param writers: dictionary {chrom_num:vcf.writer}
    :param lift_over_to_hg19: whether to liftover or not
    :param ref_data: annot data
    :return:
    """
    if compressed:
        mode = 'rb'
    else:
        mode = 'r'
    with open(vcf_file_path, mode) as opened_vcf_file:
        for line in opened_vcf_file:
            if (not line.startswith('#')) and line:
                splitted_line = line.split(None, 9)
                if lift_over_to_hg19:
                    splitted_line = lift_over_line(splitted_line, ref_data, logger)
                try:
                    writers[splitted_line[0]].write(splitted_line)
                except KeyError:
                    logger.debug('no writer for chromosome name: {}'.format(splitted_line[0]))


def lift_over_line(splitted_line: List[str], ref_data: Dict[str, RefDataRec], logger):
    axiom_id = splitted_line[2]
    try:
        ref_data_for_this_snp = ref_data[axiom_id]
        return [ref_data_for_this_snp.hg19_chr, ref_data_for_this_snp.hg19_pos] + splitted_line[2:]
    except KeyError:
        logger.debug('no ID in ref data: {}'.format(axiom_id))


def create_writers(dir_path: str, chrom_list: List[str], sample_names: List[str]) -> Dict[str, VcfWriter]:
    writers = {}
    for chrom in chrom_list:
        f_path = os.path.join(dir_path, '{}.vcf'.format(chrom))
        writers[chrom] = VcfWriter(f_path, sample_names)
    return writers


def close_writers(writers: Dict[str, VcfWriter]):
    for writer in writers.values():
        writer.close()


def write_records(writer, records: List[str]):
    for record in records[:-1]:
        writer.writen(record)
    writer.write(records[-1])
    writer.close()


def sort_vcf_file_by_pos(f_path: str, sample_names: List[str]):
    with open(f_path) as f:
        records = [line.split() for line in f if not line.startswith('#')]
        records.sort(key=lambda x: int(x[1]))
    writer = VcfWriter(f_path, sample_names)
    write_records(writer, records)


def get_sample_names(vcf_file_path: str, compressed=False) -> List[str]:
    if compressed:
        mode = 'rb'
    else:
        mode = 'r'
    with open(vcf_file_path, mode) as opened_vcf_file:
        for line in opened_vcf_file:
            if line.startswith('#CHROM'):
                return line.split()[9:]


def remove_haploid_records_from_vcf(f_path: str, sample_names: List[str], logger, start: int = None,
                                    end: int = None, separator='/'):
    assert (start is None and end is None) or (start is not None and end is not None)
    with open(f_path) as f:
        records = []
        for line in f:
            if (not line.startswith('#')) and line:
                splitted = line.split()
                if line_contains_haploid_genotypes(splitted, separator) and (
                        start is None or (int(splitted[1]) < start or int(splitted[1]) > end)):
                    logger.debug('Removing record id from vcf: {}'.format(splitted[2]))
                else:
                    records.append(splitted)
    writer = VcfWriter(f_path, sample_names)
    write_records(writer, records)


def line_contains_haploid_genotypes(splitted_line: List[str], separator: str = '/') -> bool:
    if len(splitted_line) < 9:
        print(splitted_line)
        raise
    assert splitted_line[8] == 'GT'
    return any(len(gt.split(separator)) < 2 for gt in splitted_line[9:])


def transform_haploid_to_diploid(f_path: str, sample_names: List[str], chrom='X'):
    records = []
    with open(f_path) as f:
        for line in f:
            if not line.startswith('#'):
                record = line.split()
                genotypes = [make_diploid_if_needed(gt) for gt in record[9:]]
                records.append(record[:9] + genotypes)
    writer = VcfWriter(f_path, sample_names)
    write_records(writer, records)


def make_diploid_if_needed(genotype_string, separator='/'):
    if len(genotype_string.split(separator)) == 1:
        return genotype_string + separator + genotype_string
    else:
        return genotype_string
