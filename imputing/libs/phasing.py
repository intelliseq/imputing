import os
import subprocess
import logging

COMPRESS_VCF_CMD = 'bgzip -c {} > {} 2> {}'
INDEX_COMPRESSED_VCF_CMD = 'tabix -fp vcf {} > {} 2> {}'
EAGLE_RUN_CMD = '''{} --allowRefAltSwap  --vcfTarget {}  --vcfRef {}  --outPrefix {} --geneticMapFile {} --numThreads {} >{} 2>{}'''
logger = logging.getLogger('imputing.' + __name__)


def compress_vcf(in_path: str, out_path: str, err_log_path: str, cmd_template: str = COMPRESS_VCF_CMD) -> None:
    command = cmd_template.format(in_path, out_path, err_log_path)
    logger.debug('Compressing vcf with command {}'.format(command))
    subprocess.run(command, shell=True)


def make_index_for_compressed_vcf(compressed_vcf_path: str, out_log_path: str, err_log_path: str,
                                  cmd_template: str = INDEX_COMPRESSED_VCF_CMD) -> None:
    command = cmd_template.format(compressed_vcf_path, out_log_path, err_log_path)
    logger.debug('Making  index with command {}'.format(command))
    subprocess.run(command, shell=True)


def run_eagle(target_path: str, ref_path: str, out_path: str, out_log_path: str, err_log_path: str, num_threads: int,
              genetic_map_path: str = '/home/wojtek/Eagle_v2.4.1/tables/genetic_map_hg19_withX.txt.gz',
              cmd_template: str = EAGLE_RUN_CMD, eagle_path: str = '/home/wojtek/Eagle_v2.4.1/eagle') -> None:
    command = cmd_template.format(eagle_path, target_path, ref_path, out_path, genetic_map_path, num_threads,
                                  out_log_path, err_log_path)
    logger.debug('Running eagle with command {}'.format(command))
    subprocess.run(command, shell=True)


if __name__ == '__main__':
    compress_vcf('/home/wojtek/PycharmProjects/mobigen/data/imputing/beagle_without_prephasing_in/22',
                 '/home/wojtek/PycharmProjects/mobigen/data/imputing/beagle_without_prephasing_in/22.vcf.gz')
    make_index_for_compressed_vcf(
        '/home/wojtek/PycharmProjects/mobigen/data/imputing/beagle_without_prephasing_in/22.vcf.gz')
    run_eagle(target_path='/home/wojtek/PycharmProjects/mobigen/data/imputing/beagle_without_prephasing_in/22.vcf.gz',
              ref_path='/home/wojtek/eagle2_reference/22.bcf',
              out_path='/home/wojtek/Eagle_v2.4.1/temp/22_script_vcf')
