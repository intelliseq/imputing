import subprocess
import logging
from typing import List
from typing import Union

BEAGLE_CMD_TEMPLATE = '''java -jar {} ref={} gt={} map={} out={} nthreads={} >{} 2>{}'''
logger = logging.getLogger('imputing.' + __name__)


def run_beagle(cmd_pattern: str, chromosomes: List[Union[str, int]] = list(range(1, 23)) + ['X', 'Y']) -> None:
    for chrom in chromosomes:
        cmd = cmd_pattern.format(chrom, chrom, chrom, chrom, chrom, chrom)
        logger.debug('Running beagle with command {}'.format(cmd))
        subprocess.check_call(cmd, shell=True)


def create_beagle_cmd_pattern(beagle_path: str, ref_chromosomes_path_pattern: str,
                              genotypes_for_each_chromosome_path_pattern: str, genetic_map_pattern: str,
                              out_path_pattern: str, out_log_path: str, err_log_path: str, num_threads: int,
                              beagle_cmd_template: str = BEAGLE_CMD_TEMPLATE) -> str:
    return beagle_cmd_template.format(beagle_path, ref_chromosomes_path_pattern,
                                      genotypes_for_each_chromosome_path_pattern, genetic_map_pattern, out_path_pattern,
                                      num_threads, out_log_path, err_log_path)


if __name__ == '__main__':
    beagle_cmd_pattern = create_beagle_cmd_pattern(beagle_path='/home/wojtek/Downloads/beagle.28Sep18.793.jar',
                                                   ref_chromosomes_path_pattern='/home/wojtek/ownCloud/wojtek/1000_Genomes_phase3_v5a_bref/chr{}.1kg.phase3.v5a.b37.bref3',
                                                   genotypes_for_each_chromosome_path_pattern='/home/wojtek/PycharmProjects/mobigen/data/imputing/eagle_phased/{}.vcf.gz',
                                                   genetic_map_pattern='/home/wojtek/ownCloud/wojtek/plink.GRCh37.map/plink.chr{}.GRCh37.map',
                                                   out_path_pattern='/home/wojtek/PycharmProjects/mobigen/data/imputing/beagle_eagle_phased_out/{}')
    print(beagle_cmd_pattern)
