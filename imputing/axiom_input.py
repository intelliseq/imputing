import os
import argparse
import logging
from queue import Queue
from typing import List
from typing import Dict
from typing import TextIO
from libs.experimental_input import read_id_genotype_flat_file
from libs.platform_dependent_input import read_csv
from libs.create_input import create_vcfs
from libs.create_input import SNP_data

logger = logging.getLogger('imputing')


def create_genotype_dicts_out_of_experimental_files(opened_experimental_files: List[TextIO],
                                                    first_lines_to_omit: int = 24,
                                                    char_marking_line_to_omit: str = '!') -> Dict[
    str, Dict[str, SNP_data]]:
    ret_dict = {}
    for f in opened_experimental_files:
        experimental_data = read_id_genotype_flat_file(f, first_lines_to_omit=first_lines_to_omit,
                                                       char_marking_line_to_omit=char_marking_line_to_omit)
        logger.info('Read experimental data from {}'.format(f.name))
        ret_dict[f.name] = {record.platform_dependent_id: record.genotype for record in experimental_data}
    return ret_dict


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creates vcf files out of experimental files')
    parser.add_argument('out_dir', type=str, help='output directory')
    parser.add_argument('log_dir', type=str, help='directory containing logs')
    parser.add_argument('files', type=argparse.FileType('r'), nargs='+',
                        help='''Space-separated paths to experimental files. These files should contain two columns for each SNP:
    - first containing platform-dependent id for this SNP
    - second containing exact genotype
    Example data: AX-11086525	GG''')

    parsed_args = parser.parse_args()
    out_dir = parsed_args.out_dir
    log_dir = parsed_args.log_dir
    paths_to_experimental_files = parsed_args.files

    info_queue = Queue(maxsize=2)
    try:
        os.makedirs(log_dir)
    except FileExistsError:
        info_queue.put('{} already exists'.format(log_dir))
    try:
        os.makedirs(out_dir)
    except FileExistsError:
        info_queue.put('{} already exists'.format(out_dir))

    log_file = os.path.join(log_dir, '{}.log'.format(os.path.basename(__file__).split('.')[0]))
    # setting up logger
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    while not info_queue.empty():
        logger.debug(info_queue.get())
    logger.info('transforming experimental files to genotype  dictionaries')
    experimental_data = create_genotype_dicts_out_of_experimental_files(paths_to_experimental_files)
    logger.info('reading and transforming platform-dependent data')
    # platform_dependent_data = read_csv('/app/data/Axiom_GW_Hu_SNP.na35.annot.csv') #TODO change this and following line
    platform_dependent_data = read_csv(
        '/home/wojtek/Pobrane/Axiom_PMDA.na36.r6.a8.annot.csv/Axiom_PMDA.na36.r6.a8.annot.csv')
    platform_dependent_data_dict = {
        record.platform_dependent_id: SNP_data(record.rs_id, record.chrom, record.position, record.strand,
                                               record.ref_allele,
                                               record.alt_allele) for record in platform_dependent_data}
    logger.info('creating vcfs')
    create_vcfs(experimental_data, platform_dependent_data_dict, out_dir)
