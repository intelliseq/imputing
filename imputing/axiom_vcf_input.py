import os
import argparse
import logging
from joblib import Parallel, delayed
from libs import const_hg19
from libs import const_hg38
from libs.vcf_processing import RefDataRec
from libs.vcf_processing import curate_vcf
from libs.vcf_processing import create_writers
from libs.vcf_processing import split_vcf_by_chromosomes
from libs.vcf_processing import get_sample_names
from libs.vcf_processing import close_writers
from libs.vcf_processing import remove_haploid_records_from_vcf
from libs.vcf_processing import sort_vcf_file_by_pos
from libs.vcf_processing import transform_haploid_to_diploid
from queue import Queue
from typing import Dict


def read_ref_data(ref_file_path: str) -> Dict[str, RefDataRec]:
    ref_data = {}
    with open(ref_file_path) as opened_ref_file:
        for line in opened_ref_file:
            if not (line.startswith('#') or line.startswith('"Probe')):
                splitted = [x.strip('"') for x in line.strip().split('","')]
                ref_data[splitted[0]] = RefDataRec(splitted[0], splitted[2], splitted[4], splitted[5], splitted[7],
                                                   splitted[11], splitted[12], splitted[13], splitted[14], splitted[46],
                                                   splitted[47], splitted[48])
    return ref_data


def remove_haploid_records_from_vcf_wrapper(path_template: str, chrom: str, sample_names, logger):
    f_path = path_template.format(chrom)
    remove_haploid_records_from_vcf(f_path, sample_names, logger=logger)


def sort_vcf_file_by_pos_wrapper(path_template: str, chrom: str, sample_names):
    f_path = path_template.format(chrom)
    sort_vcf_file_by_pos(f_path, sample_names)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='splits experimental vcf file to individual chromosomes and sorts them')
    parser.add_argument('in_file', type=str, help='input vcf file')
    parser.add_argument('out_dir', type=str, help='output directory')
    parser.add_argument('--annot_file', type=str, help='annotation csv file',
                        default='/app_data/Axiom_PMDA.na36.r6.a8.annot.csv')
    parser.add_argument('--log_dir', type=str, help='directory containing logs')
    parser.add_argument('--hg19', action='store_true', default=False, help='Liftover to hg19')
    parser.add_argument('--jobs', type=int, default=1, help='Max number of processes to be run in parallel')
    parsed_args = parser.parse_args()

    out_dir = os.path.expanduser(parsed_args.out_dir)
    log_dir = os.path.expanduser(parsed_args.log_dir)
    in_file = os.path.expanduser(parsed_args.in_file)
    annot_file = os.path.expanduser(parsed_args.annot_file)

    info_queue = Queue(maxsize=2)
    try:
        os.makedirs(log_dir)
    except FileExistsError:
        info_queue.put('{} already exists'.format(log_dir))
    try:
        os.makedirs(out_dir)
    except FileExistsError:
        info_queue.put('{} already exists'.format(out_dir))

    log_file = os.path.join(log_dir, '{}.log'.format(os.path.basename(__file__).split('.')[0]))
    # setting up logger
    logger = logging.getLogger('imputing')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    while not info_queue.empty():
        logger.debug(info_queue.get())

    if parsed_args.hg19:
        par1_end = const_hg19.par1_end
        par2_start = const_hg19.par2_start
    else:
        par1_end = const_hg38.par1_end
        par2_start = const_hg38.par2_start

    logger.info('Curate input file')
    curated_file_path = os.path.join(out_dir, 'curated_input_vcf.vcf')
    sample_names = get_sample_names(in_file)
    curate_vcf(in_file, curated_file_path, sample_names)

    logger.info('Reading annotation file')
    ref_data = read_ref_data(annot_file)

    logger.info('Splitting vcf by chromosome')
    if parsed_args.hg19:
        logger.info('Will perform liftover to hg19 while splitting')
    autosomes_list = [str(x) for x in range(1, 23)]
    autosomes_shuffled = []
    for i in range(len(autosomes_list) // 2):
        autosomes_shuffled.append(autosomes_list[i])
        autosomes_shuffled.append(autosomes_list[-i - 1])
    chrom_list = autosomes_shuffled + ['X', 'Y', 'MT']

    writers = create_writers(out_dir, chrom_list, sample_names)
    split_vcf_by_chromosomes(curated_file_path, writers, parsed_args.hg19, ref_data, logger)
    close_writers(writers)

    logger.info('Removing haploid records from autosomes')
    Parallel(n_jobs=parsed_args.jobs)(
        delayed(remove_haploid_records_from_vcf_wrapper)(os.path.join(out_dir, '{}.vcf'), chrom, sample_names, logger)
        for chrom in autosomes_list)

    logger.info('Removing haploid records from chromX PAR regions')
    f_path = os.path.join(out_dir, 'X.vcf')
    remove_haploid_records_from_vcf(f_path, sample_names, logger, par1_end, par2_start)
    logger.info('Preparing chromX')
    transform_haploid_to_diploid(f_path, sample_names, chrom='X')

    logger.info('Sorting chromosomes')
    Parallel(n_jobs=parsed_args.jobs)(
        delayed(sort_vcf_file_by_pos_wrapper)(os.path.join(out_dir, '{}.vcf'), chrom, sample_names) for chrom in
        chrom_list)
