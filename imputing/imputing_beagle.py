import logging
import argparse
import os
import sys
from queue import Queue
from libs.imputing import run_beagle
from libs.imputing import create_beagle_cmd_pattern

__version__ = '1.3'


def is_autosome(chrom: str):
    try:
        chrom_int = int(chrom)
    except ValueError:
        return False
    return 0 < chrom_int < 23


def is_heterochromosome(chrom: str):
    return chrom == 'X'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Performs imputing with beagle.28Sep18.793')
    parser.add_argument('in_dir', type=str, help='directory containing phased genotypes')
    parser.add_argument('out_dir', type=str, help='directory containing output files (imputed genotypes)')
    parser.add_argument('log_dir', type=str, help='directory for logs')
    parser.add_argument('--beagle_threads', type=int, default=1, help='Number of threads to be used by beagle')
    parser.add_argument('--beagle_ref_data_dir', type=str, default='/app_data/beagle_ref_data/',
                        help='Directory containing beagle reference data')
    parser.add_argument('--beagle_genetic_maps_dir', type=str, default='/app_data/beagle_genetic_maps/',
                        help='Directory containing beagle genetic maps')
    parser.add_argument('--beagle_path', type=str, default='/app/bin/beagle.16May19.351.jar', help='Path to beagle jar')
    parser.add_argument('--hg19', action='store_true', default=False, help='Use hg19 data')
    parser.add_argument('--chromosomes',
                        default=str(list(range(1, 23)) + ['X']).replace(' ', '').replace("'", '')[1:-1],
                        help='List of chromosomes')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parsed_args = parser.parse_args()

    build = 19 if parsed_args.hg19 else 38
    chrom_fname_patterns = {19: 'chr{}.1kg.phase3.v5a.b37.bref3', 38: '{}.38.bref3'}
    chromX_fname_patterns = {19: 'chr{}.1kg.phase3.v5a.b37.bref3', 38: '{}.38.vcf.gz'}
    map_fname_patterns = {19: 'plink.chr{}.GRCh37.map', 38: 'plink.chr{}.GRCh38.map'}

    out_dir = parsed_args.out_dir
    in_dir = parsed_args.in_dir
    log_dir = parsed_args.log_dir

    info_queue = Queue()
    for directory in [out_dir, log_dir]:
        try:
            os.makedirs(directory)
        except FileExistsError:
            info_queue.put('{} already exists'.format(directory))

    log_file = os.path.join(log_dir, '{}.log'.format(os.path.basename(__file__).split('.')[0]))
    # setting up logger
    logger = logging.getLogger('imputing')
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    while not info_queue.empty():
        logger.debug(info_queue.get())

    logger.info('imputing')

    autosomes = [x for x in parsed_args.chromosomes.split(',') if is_autosome(x)]
    heterochromosomes = [x for x in parsed_args.chromosomes.split(',') if is_heterochromosome(x)]

    # autosomes
    beagle_cmd_pattern = create_beagle_cmd_pattern(beagle_path=parsed_args.beagle_path,
                                                   ref_chromosomes_path_pattern=os.path.join(
                                                       parsed_args.beagle_ref_data_dir,
                                                       chrom_fname_patterns[build]),
                                                   genotypes_for_each_chromosome_path_pattern=os.path.join(
                                                       in_dir, '{}_phased.vcf.gz'),
                                                   genetic_map_pattern=os.path.join(parsed_args.beagle_genetic_maps_dir,
                                                                                    map_fname_patterns[build]),
                                                   out_path_pattern=os.path.join(out_dir,
                                                                                 '{}_imputed'),
                                                   num_threads=parsed_args.beagle_threads,
                                                   out_log_path=os.path.join(log_dir, '{}_out'),
                                                   err_log_path=os.path.join(log_dir, '{}_err'))
    run_beagle(beagle_cmd_pattern, chromosomes=autosomes)

    # chromX
    if heterochromosomes:
        beagle_x_cmd_pattern = create_beagle_cmd_pattern(beagle_path=parsed_args.beagle_path,
                                                         ref_chromosomes_path_pattern=os.path.join(
                                                             parsed_args.beagle_ref_data_dir,
                                                             chromX_fname_patterns[build]),
                                                         genotypes_for_each_chromosome_path_pattern=os.path.join(in_dir,
                                                                                                                 '{}_phased.vcf.gz'),
                                                         genetic_map_pattern=os.path.join(
                                                             parsed_args.beagle_genetic_maps_dir,
                                                             map_fname_patterns[build]),
                                                         out_path_pattern=os.path.join(out_dir, '{}_imputed'),
                                                         num_threads=parsed_args.beagle_threads,
                                                         out_log_path=os.path.join(log_dir, '{}_out'),
                                                         err_log_path=os.path.join(log_dir, '{}_err'))
        run_beagle(beagle_x_cmd_pattern, chromosomes=['X'])
