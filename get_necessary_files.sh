#!/usr/bin/env bash

if [[ "$1" != "" ]]; then
  cd $1
fi

echo "Creating directories"
if ! [[ -d "bin" ]]; then
  mkdir bin
fi
if ! [[ -d "data" ]]; then
  mkdir data
fi

echo "Getting binaries"
cd bin
if ! [[ -f beagle.28Sep18.793.jar ]]; then
  wget https://faculty.washington.edu/browning/beagle/beagle.28Sep18.793.jar
fi
aria2c -x4 https://data.broadinstitute.org/alkesgroup/Eagle/downloads/Eagle_v2.4.1.tar.gz
tar -xvzf Eagle_v2.4.1.tar.gz
rm Eagle_v2.4.1.tar.gz
mv Eagle_v2.4.1/eagle .
mv Eagle_v2.4.1/tables/genetic_map_hg19_withX.txt.gz ../data/
rm -rf Eagle_v2.4.1/
cd ..

echo "Getting reference data"
cd data
if [[ -d "beagle_genetic_maps" ]]; then
  rm -f beagle_genetic_maps/*
else
  mkdir beagle_genetic_maps
fi
cd beagle_genetic_maps
aria2c -x4 http://bochet.gcc.biostat.washington.edu/beagle/genetic_maps/plink.GRCh37.map.zip
unzip plink.GRCh37.map.zip
rm plink.GRCh37.map.zip
cd ..

echo "Downloading beagle reference data"
if ! [[ -d "beagle_ref_data" ]]; then
  mkdir beagle_ref_data
fi
cd beagle_ref_data
for chr in {1..22} X; do
  fname=chr${chr}.1kg.phase3.v5a.b37.bref3
  if ! [[ -f ${fname} ]]; then
    aria2c -x4 http://bochet.gcc.biostat.washington.edu/beagle/1000_Genomes_phase3_v5a/b37.bref3/${fname}
  fi
done
cd ..

if ! [[ -d "eagle_ref_data" ]]; then
  mkdir eagle_ref_data
fi
cd eagle_ref_data
echo "Downloading eagle reference data and converting from .vcf to .bcf"
if ! [[ -f "X.bcf" ]]; then
  aria2c -x4 ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz
  bcftools view -Ob ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz > X.bcf
  rm ALL.chrX.phase3_shapeit2_mvncall_integrated_v1b.20130502.genotypes.vcf.gz
fi
if ! [[ -f "X.bcf.csi" ]]; then
  bcftools index -c X.bcf
fi

for chr in {1..22}; do
  if ! [[ -f ${chr}.bcf ]]; then
      aria2c -x4 ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chr${chr}.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
      bcftools view -Ob ALL.chr${chr}.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz >${chr}.bcf
      rm ALL.chr${chr}.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf.gz
  fi
  if ! [[ -f ${chr}.bcf.csi ]]; then
    bcftools index -c ${chr}.bcf
  fi
done

#wget ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/release/20130502/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz
#bcftools view -Ob ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz > Y.bcf
#rm ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz
#bcftools index -c Y.bcf

cd ..

#echo "Getting illumina data"
#if ! [[ -f "Axiom_GW_Hu_SNP.na35.annot.csv" ]]; then
#    aria2c -x4 http://media.affymetrix.com/analysis/downloads/na35/genotyping/Axiom_GW_Hu_SNP.na35.annot.csv.zip
#    unzip Axiom_GW_Hu_SNP.na35.annot.csv.zip
#    rm Axiom_GW_Hu_SNP.na35.annot.csv.zip
#fi