from __future__ import print_function
from Bio import Entrez
import argparse
import json
import sys
import urllib
import xml.etree.ElementTree as ET
from typing import Dict
from typing import Tuple


def get_info_using_location(location: str, organism: str) -> Dict:
    rs_id = get_rsid_from_location(location, organism)
    if not rs_id:
        return {}
    return get_info_using_rsid(rs_id)


def get_rsid_from_location(location: str, organism: str) -> str:
    term_template = "{}[Chromosome]) AND {}[Base Position]) AND {}[Organism]"
    location_data = split_location_string(location)
    found = Entrez.esearch(db='snp',
                           term=term_template.format(location_data['chrom'], location_data['position'], organism),
                           retmode='json')
    res = json.loads(found.read())["esearchresult"]
    if res['count'] != '1':
        print('Your query returned {} rsids. Exiting.'.format(res['count']), file=sys.stderr)
        return None
    else:
        return 'rs{}'.format(res['idlist'][0])


def split_location_string(location: str) -> Dict[str, str]:
    chrom, pos = location.split(':')
    return {'chrom': chrom, 'position': pos}


def get_info_using_rsid(rsid: str) -> Dict:
    content = connect_to_dbsnp_and_get_content(rsid)
    ret_dict = {'rsid': rsid}
    ret_dict.update(process_xml(content))
    return ret_dict


def connect_to_dbsnp_and_get_content(rsid: str) -> str:
    while True:
        try:
            handle = Entrez.efetch(db="snp", id=rsid, report='xml')
            content = handle.read()
            return content
        except urllib.error.HTTPError:
            pass


def process_xml(xml: str) -> Dict:
    root = ET.fromstring(xml)
    rs = root.find('{https://www.ncbi.nlm.nih.gov/SNP/docsum}Rs')
    if not rs:
        return {}
    alleles = rs.find(
        '{https://www.ncbi.nlm.nih.gov/SNP/docsum}Sequence/{https://www.ncbi.nlm.nih.gov/SNP/docsum}Observed').text
    assembly = rs.find('{https://www.ncbi.nlm.nih.gov/SNP/docsum}Assembly')
    if not assembly:
        return {}
    assert assembly.attrib['genomeBuild'].startswith("38") and assembly.attrib['current'] == 'true' and assembly.attrib[
        'reference'] == 'true'
    component = assembly.find('{https://www.ncbi.nlm.nih.gov/SNP/docsum}Component')
    map_loc = component.find('{https://www.ncbi.nlm.nih.gov/SNP/docsum}MapLoc')
    ref_allele_initial = map_loc.attrib['refAllele']
    orientation = map_loc.attrib['orient'].strip()
    position = int(map_loc.attrib['physMapInt'])
    chrom = component.attrib['chromosome']
    ref_allele, alt_alleles = parse_alleles(alleles, ref_allele_initial, orientation)
    if alt_alleles != '-':
        position += 1
    return {'chrom': chrom, 'position': position, 'ref': ref_allele, 'alt': alt_alleles}


def parse_alleles(alleles_str: str, ref_allele: str, orientation: str) -> Tuple[str]:
    alleles = alleles_str.split('/')
    if orientation == 'reverse':
        alleles = [reverse_complement(x) for x in alleles]
    alt_alleles = [x for x in alleles if not x == ref_allele]
    return ref_allele, ','.join(alt_alleles)


def reverse_complement(dna: str):
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'Y': 'R', 'R': 'Y', 'S': 'W', 'W': 'S', 'K': 'M',
                  'M': 'K', 'B': 'V', 'V': 'B', 'D': 'H', 'H': 'D', 'N': 'N', '-': '-'}
    seq = dna.upper()
    return ''.join(complement[base] for base in reversed(seq))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Returns json containing basic info from dbsnp based on either rsID or location')
    parser.add_argument('--location', type=str, help="Location to be checked in format chr:pos", default='')
    parser.add_argument('--rsid', type=str, help='rsID to be checked', default='')
    parser.add_argument('--email', type=str, help='Provide your email address in case NCBI would like to contact you',
                        default='A.N.Other@example.com')
    parser.add_argument('--organism', type=str, help='Provide organism while searching dbsnp with location data',
                        default='"Homo sapiens"')
    args = parser.parse_args()
    if (args.location and args.rsid) or not (args.location or args.rsid):
        raise RuntimeError('You must provide EITHER location OR rsID')
    Entrez.email = args.email
    if args.rsid:
        print(json.dumps(get_info_using_rsid(args.rsid)))
    else:
        print(json.dumps(get_info_using_location(args.location, args.organism)))
