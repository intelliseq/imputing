import argparse
import os
import re
import subprocess
import json
from typing import Set
from typing import Pattern
from typing import List
from typing import Dict
from typing import Union

RSID_RE = re.compile('rs\d+')

DESCRIPTION = '''Removes lines from vcf files.
When a line contains invalid lication, the script redirects the line to another file
(default /dev/null)'''


def adjust_file_path(path: str) -> str:
    return os.path.abspath(os.path.expanduser(path))


def read_whitespace_separated(path: str) -> Set[str]:
    with open(path) as f:
        return set(f.read().split())


def is_proper_rsid(rsid: str, rsid_re: Pattern = RSID_RE) -> bool:
    found = rsid_re.search(rsid)
    if found:
        return found.start() == 0 and found.end() == len(rsid)
    return False


def if_valid_return_content(p_err: str, p_out: str) -> bool:
    if p_err.strip():
        return False
    res = json.loads(p_out)
    try:
        ref = res["ref"]
        alt = res['alt']
        if is_indel(ref, alt):
            return False
        else:
            return res
    except KeyError:
        return False


def is_indel(ref: str, alt: str) -> bool:
    if '-' in ref or '-' in alt:
        return True
    else:
        return False


def same_num_of_alleles(str1: str, str2: str) -> bool:
    return len(str1.split(',')) == len(str2.split(','))


def repeating_elements_present(container: List[str]):
    return len(container) != len(set(container))


def invalid_chars_present(to_check: str, allowed={'A', 'C', 'T', 'G', 'N', ','}):
    chars = set(to_check)
    return chars - allowed


def modify_vcf_line(splitted_original_line: List[str], rs_info: Dict[str, Union[str, int]]) -> str:
    chrom, position, rsid, ref, alt, rest_of_the_line = splitted_original_line
    return '\t'.join(
        [rs_info['chrom'], str(rs_info['position']), rs_info['rsid'], rs_info['ref'], rs_info['alt'], rest_of_the_line])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('infile', type=str, help='vcf to be processed')
    parser.add_argument('outfile', type=str, help='output file')
    parser.add_argument('to_check', type=str,
                        help='input file containing whitespace-separated values to be checked and possibly excluded')
    parser.add_argument('--invalid_out', type=str, help='outfile for invalid lines', default='/dev/null')
    parser.add_argument('--python_path', type=str, default='/usr/bin/python3.6')
    # parser.add_argument('--input_gzipped', action='store_true', default=False)
    # parser.add_argument('--output_gzipped', action='store_true', default=False)
    args = parser.parse_args()

    values_to_be_checked = read_whitespace_separated(adjust_file_path(args.to_check))
    values_already_checked = set()

    with open(adjust_file_path(args.infile)) as fr, open(adjust_file_path(args.outfile), 'w') as fw, open(
            adjust_file_path(args.invalid_out), 'w') as f_invalid:
        for line in fr:
            splitted = line.split(None, 5)
            if line.startswith('#'):
                fw.write(line)
            elif invalid_chars_present(splitted[4]) or invalid_chars_present(splitted[3]) or splitted[
                1] in values_already_checked or repeating_elements_present(splitted[4].split(',') + [splitted[3]]):
                f_invalid.write(line)
            elif splitted[1] not in values_to_be_checked:
                fw.write(line)
            else:
                values_already_checked.add(splitted[1])
                chrom, location = splitted[:2]
                directory = os.path.dirname(adjust_file_path(__file__))
                if is_proper_rsid(splitted[2]):
                    command = [args.python_path, '{}/get_info_from_db_snp.py'.format(directory), '--rsid', splitted[2]]
                else:
                    command = [args.python_path, '{}/get_info_from_db_snp.py'.format(directory), '--location',
                               '{}:{}'.format(chrom, location)]
                command = ' '.join(command) + ' --email wojciech.galan@intelliseq.pl'
                p = subprocess.run(command, shell='/bin/bash', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                content = if_valid_return_content(p.stderr, p.stdout)
                if not content or not same_num_of_alleles(splitted[4], content['alt']) or repeating_elements_present(
                        content['alt']):
                    f_invalid.write(line)
                else:
                    fw.write(modify_vcf_line(splitted, content))
